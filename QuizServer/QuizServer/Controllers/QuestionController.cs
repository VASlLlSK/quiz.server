﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using QuizServer.Models;

namespace QuizServer.Controllers
{

    public class QuestionController : ApiController
    {
        public QuestionController()
        {
            DB = new DataBase();
        }
        public DataBase DB { get; set; }

        // POST api/values/5
        [Route("api/Question/{category}")]
        public async Task<Question> Post([FromBody] string name, string category)
        {
            Question result;
            return await Task.Run(() =>
            {
                DB.OpenConnection();
                result = DB.GetQuestionFromCategory(category, name);
                DB.CloseConnection();

                return result;
            });
        }

        // PUT api/values/5
        [Route("api/Question/{idQuestion}/{answer}")]
        public async Task<bool> Put(int idQuestion, string answer,[FromBody] string userName)
        {
            bool f = false;
            return await Task.Run(() =>
            {
                DB.OpenConnection();
                var result = DB.GetTrueAnswer(idQuestion);

                if (result == answer)
                    f = true;

                var user = DB.GetUser(userName);

                if (user == null)
                    return false;

                if (f)
                    user.score++;
                else
                    user.score--;

                DB.EditScore(user);

                DB.CloseConnection();
                return f;
            });
        }
    }
}