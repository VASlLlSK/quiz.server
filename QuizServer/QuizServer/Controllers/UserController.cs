﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using QuizServer.Models;

namespace QuizServer.Controllers
{
    public class UserController : ApiController
    {
        public UserController()
        {
            DB = new DataBase();
        }
        public DataBase DB { get; set; }

        // GET api/RatingTop10
        [Route("api/RatingTop10")]
        public async Task<List<User>> Get()
        {
            List<User> result = null;
            await Task.Run(() =>
            {
                DB.OpenConnection();
                result = DB.GetRatingTop10();
                DB.CloseConnection();
            });
            return result;
        }

        // POST api/User
        [Route("api/User")]
        public async Task<bool> POST([FromBody] string name)
        {
            bool result = false;

            await Task.Run(() =>
            {
                DB.OpenConnection();
                result = DB.NameIsFree(name);

                if (result)
                    DB.SetUser(name);

                DB.CloseConnection();
            });

            return result;
        }
    }
}