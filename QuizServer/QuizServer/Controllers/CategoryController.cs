﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using QuizServer.Models;

namespace QuizServer.Controllers
{
    public class CategoryController : ApiController
    {
        public CategoryController()
        {
            DB = new DataBase();
        }
        public DataBase DB { get; set; }

        // GET api/Category
        public async Task<List<Category>> Get()
        {
            List<Category> result = null;

            await Task.Run(() =>
            {
                DB.OpenConnection();
                result = DB.GetAllCategory();
                DB.CloseConnection();
            });

            return result;
        }
    }
}
