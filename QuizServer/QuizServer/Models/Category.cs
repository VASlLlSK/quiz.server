﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuizServer.Models
{
    public class Category
    {
        public int id { get; set; }
        public string categoryName { get; set; }
        public string category { get; set; }
    }
}