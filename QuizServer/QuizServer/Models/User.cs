﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuizServer.Models
{
    public class User
    {
        public int id { get; set; }
        public string name { get; set; }
        public int score { get; set; }
        public string completedQuestions { get; set; }
        public User()
        {
            score = 0;
            completedQuestions = "";
        }
    }

}