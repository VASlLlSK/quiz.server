﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using Dapper;
using System.Runtime.CompilerServices;

namespace QuizServer.Models
{
    public class DataBase
    {
        string _connectionString = "Server=localhost;Port=3306;Database=quiz;Uid=root;Pwd=54321ASASAS;";
        private MySqlConnection connection;
        public DataBase()
        {
            connection = new MySqlConnection(_connectionString);
        }

        public MySqlConnection GetConnection()
        {
            return connection;
        }

        public void OpenConnection()
        {
            if (connection.State == System.Data.ConnectionState.Closed)
                connection.Open();
        }

        public void CloseConnection()
        {
            if (connection.State == System.Data.ConnectionState.Open)
                connection.Close();
        }

        public List<User> GetAllUsers()
        {
            List<User> users = new List<User>();

            var sql = "SELECT * FROM quiz.users";

            users = connection.Query<User>(sql).ToList();
            return users;
        }

        public List<Category> GetAllCategory()
        {
            List<Category> categories = new List<Category>();

            var sql = "SELECT * FROM quiz.categories";

            categories = connection.Query<Category>(sql).ToList();
            return categories;
        }

        public User GetUser(string name)
        {
            User user = null;
            var sql = "SELECT * FROM quiz.users WHERE Name = @name";
            user = connection.Query<User>(sql, new { name = name }).FirstOrDefault();
            return user;
        }

        public Question GetQuestionFromCategory(string category, string name)
        {
            var user = GetUser(name);

            if (user == null)
                return null;


            var sql = "SELECT COUNT(*) FROM quiz.questions WHERE Category = @category";
            var count = connection.Query<int>(sql, new { Category = category }).First();

            sql = "SELECT * FROM quiz.questions WHERE Category = @category";
            var questions = connection.Query<Question>(sql, new { Category = category }).ToList();

            for (int i=0; i < count; i++)
            {
                var o = user.completedQuestions.Contains(" " + questions[i].id.ToString() + ",");
                if (o == false)
                {
                    user.completedQuestions += " " + questions[i].id + ",";

                    sql = "UPDATE quiz.users SET CompletedQuestions = @completedQuestions WHERE id = @id";
                    connection.Query<Question>(sql, new { id = user.id, completedQuestions = user.completedQuestions});

                    return questions[i];
                }
            }

            return null;
        }

        public void EditScore(User u)
        {
            var sql = "UPDATE quiz.users SET Score = @score WHERE id = @id";
            connection.Query<User>(sql, new { id = u.id, score = u.score});
        }

        public string GetTrueAnswer(int id)
        {
            string result;

            var sql = "SELECT TrueAnswer FROM quiz.questions WHERE id = @id";

            result = connection.Query<string>(sql, new { id = id}).First();
            return result;
        }

        public List<User> GetRatingTop10()
        {
            var sql = "SELECT * FROM quiz.users ORDER BY Score DESC, Score DESC LIMIT 10";
            var result = connection.Query<User>(sql).ToList();
            return result;
        }

        public bool NameIsFree(string name)
        {
            if (name != null)
            {
                User result = null;
                var sql = "SELECT * FROM quiz.users WHERE Name = @name";
                result = connection.Query<User>(sql, new { name = name }).FirstOrDefault();
                if (result == null)
                    return true;
            }

            return false;
        }

        public int SetUser(string name)
        {
            if (name != null)
            {
                var sql = "INSERT INTO quiz.users (Name, Score) VALUES (@name, @score)";

                connection.Query<int>(sql, new { name = name, score = 0 });

                sql = "SELECT id FROM quiz.users WHERE id = LAST_INSERT_ID()";
                int id = connection.Query<int>(sql).First();
                    return id;
            }
            return 0;
        }
    }
}
