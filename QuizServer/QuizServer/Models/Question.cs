﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuizServer.Models
{
    public class Question
    {
        public int id { get; set; }
        public string category { get; set; }
        public string question { get; set; }
        public string AnswerA { get; set; }
        public string AnswerB { get; set; }
        public string AnswerC { get; set; }
        public string AnswerD { get; set; }
    }
}